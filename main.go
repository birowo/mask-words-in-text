package main

func words(slc []byte, cb func(int, int)) {
	slcLen := len(slc)
	for end := 0; end < slcLen; end++ {
		for end < slcLen && slc[end] == ' ' {
			end++
		}
		bgn := end
		for end < slcLen && slc[end] != ' ' {
			end++
		}
		cb(bgn, end)
	}
}
func main() {
	txt := []byte(" dasar   kau   jahat   sekali   sangat   jahat  hfh ")
	println(string(txt))
	words(txt, func(bgn, end int) {
		//println(string(txt[bgn:end]))
		switch string(txt[bgn:end]) {
		case "jahat", "kau":
			for i := bgn; i < end; i++ {
				txt[i] = '*'
			}
		}
	})
	println(string(txt))
}
